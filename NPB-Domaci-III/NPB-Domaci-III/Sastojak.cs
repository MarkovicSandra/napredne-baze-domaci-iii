﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPB_Domaci_III
{
    public class Sastojak
    {
        [BsonElement]
        public string Naziv { get; set; }

        [BsonElement]
        public string Kolicina { get; set; }
    }
}
