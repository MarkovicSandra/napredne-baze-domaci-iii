﻿using NPB_Domaci_III.Data_Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NPB_Domaci_III
{
    public partial class UpdateMeni : Form
    {
        DataProvider dataProvider = new DataProvider();
        private string NazivMenija;
        public UpdateMeni()
        {
            InitializeComponent();
        }

        public UpdateMeni(Meni meni)
        {
            InitializeComponent();
            PopuniComboBoxeve();
            textBoxNaziv.Text = meni.Naziv;
            NazivMenija = meni.Naziv;
            if (!string.IsNullOrEmpty(meni.Predjelo))
            {
                comboBoxPredjelo.SelectedItem = meni.Predjelo;
            }

            if (!string.IsNullOrEmpty(meni.GlavnoJelo))
            {
                comboBoxGlavnoJelo.SelectedItem = meni.GlavnoJelo;
            }

            if (!string.IsNullOrEmpty(meni.Salata))
            {
                comboBoxSalata.SelectedItem = meni.Salata;
            }

            if (!string.IsNullOrEmpty(meni.Dezert))
            {
                comboBoxDezert.SelectedItem = meni.Dezert;
            }

            textBoxPice.Text = meni.Pice;
        }

        private void buttonSacuvaj_Click(object sender, EventArgs e)
        {
            if (ProveriObaveznaPolja())
            {
                string naziv = textBoxNaziv.Text;
                string predjelo = String.Empty;
                string glavnoJelo = String.Empty;
                string salata = String.Empty;
                string dezert = String.Empty;

                if (comboBoxPredjelo.SelectedIndex != -1)
                {
                    predjelo = comboBoxPredjelo.SelectedItem.ToString();
                }

                if (comboBoxGlavnoJelo.SelectedIndex != -1)
                {
                    glavnoJelo = comboBoxGlavnoJelo.SelectedItem.ToString();
                }

                if (comboBoxSalata.SelectedIndex != -1)
                {
                    salata = comboBoxSalata.SelectedItem.ToString();
                }

                if (comboBoxDezert.SelectedIndex != -1)
                {
                    dezert = comboBoxDezert.SelectedItem.ToString();
                }


                string pice = textBoxPice.Text;

                Meni meni = new Meni()
                {
                    Naziv = naziv,
                    Predjelo = predjelo,
                    GlavnoJelo = glavnoJelo,
                    Dezert = dezert,
                    Salata = salata,
                    Pice = pice
                };

                dataProvider.UpdateMeni(meni, NazivMenija);

                this.Close();
            }
        }

        private void PopuniComboBoxeve()
        {
            var predjela = dataProvider.VratiSvaPredjela();
            var glavnaJela = dataProvider.VratiSvaGlavnaJela();
            var salate = dataProvider.VratiSveSalate();
            var dezerti = dataProvider.VratiSveDezerte();

            foreach (var predjelo in predjela)
                comboBoxPredjelo.Items.Add(predjelo);

            foreach (var glavnoJelo in glavnaJela)
                comboBoxGlavnoJelo.Items.Add(glavnoJelo);

            foreach (var salata in salate)
                comboBoxSalata.Items.Add(salata);

            foreach (var dezert in dezerti)
                comboBoxDezert.Items.Add(dezert);
        }

        private bool ProveriObaveznaPolja()
        {
            var naziv = textBoxNaziv.Text;

            if (String.IsNullOrWhiteSpace(naziv))
            {
                MessageBox.Show("Morate uneti naziv menija!");
                return false;
            }

            if (comboBoxPredjelo.SelectedIndex == -1 && comboBoxGlavnoJelo.SelectedIndex == -1
                && comboBoxSalata.SelectedIndex == -1 && comboBoxDezert.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati bar jedno jelo!");
                return false;
            }

            if (naziv != NazivMenija && dataProvider.PostojiMeni(naziv))
            {
                MessageBox.Show("Meni sa datim nazivom postoji u bazi!");
                return false;
            }

            return true;
        }
    }
}
