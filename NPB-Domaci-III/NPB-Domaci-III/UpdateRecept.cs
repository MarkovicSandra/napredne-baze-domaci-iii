﻿using NPB_Domaci_III.Data_Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NPB_Domaci_III
{
    public partial class UpdateRecept : Form
    {
        public DataProvider dataProvider = new DataProvider();
        private string NazivRecepta;
        public UpdateRecept()
        {
            InitializeComponent();
        }

        public UpdateRecept(Recept recept)
        {
            InitializeComponent();
            NazivRecepta = recept.Naziv;
            textBoxNaziv.Text = recept.Naziv;
            numericUpDownBrojOsoba.Value = recept.BrojOsoba;
            numericUpDownVremePripreme.Value = recept.VremePripreme;
            textBoxNacinPripreme.Text = recept.NacinPripreme;
            comboBoxKategorija.SelectedItem = recept.Kategorija;
            int i = 0;
            dataGridViewNamirnice.Rows.Add(recept.Namirnice.Count);
            foreach (var namirnica in recept.Namirnice)
            {
                dataGridViewNamirnice.Rows[i].Cells["Naziv"].Value = namirnica.Naziv;
                dataGridViewNamirnice.Rows[i].Cells["Kolicina"].Value = namirnica.Kolicina;
                i++;
            }
        }

        private void buttonSacuvaj_Click(object sender, EventArgs e)
        {
            if (ProveriObaveznaPolja())
            {
                var naziv = textBoxNaziv.Text;
                var brojOsoba = numericUpDownBrojOsoba.Value;
                var vremePripreme = numericUpDownVremePripreme.Value;
                var nacinPripreme = textBoxNacinPripreme.Text;
                var namirnice = VratiSvePopunjeneRedoveIzTabele();
                var kategorija = comboBoxKategorija.SelectedItem.ToString();

                Recept recept = new Recept()
                {
                    Naziv = naziv,
                    Kategorija = kategorija,
                    VremePripreme = Convert.ToInt32(vremePripreme),
                    NacinPripreme = nacinPripreme,
                    BrojOsoba = Convert.ToInt32(brojOsoba),
                    Namirnice = namirnice
                };

                dataProvider.UpdateRecept(recept, NazivRecepta);

                this.Close();
            }
        }

        private bool ProveriObaveznaPolja()
        {
            var naziv = textBoxNaziv.Text;
            var rows = dataGridViewNamirnice.Rows;

            if (String.IsNullOrWhiteSpace(naziv))
            {
                MessageBox.Show("Morate uneti naziv recepta!");
                return false;
            }

            if (comboBoxKategorija.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati kategoriju recepta!");
                return false;
            }

            if (naziv != NazivRecepta && dataProvider.PostojiRecept(naziv))
            {
                MessageBox.Show("Recept sa datim nazivom postoji u bazi!");
                return false;
            }

            foreach (DataGridViewRow row in rows)
            {
                if (ProveriPopunjenRedUTabeli(row))
                    return true;
            }

            MessageBox.Show("Morate uneti jednu namirnicu!");
            return false;
        }

        private List<Sastojak> VratiSvePopunjeneRedoveIzTabele()
        {
            var lista = new List<Sastojak>();
            foreach (DataGridViewRow row in dataGridViewNamirnice.Rows)
            {
                if (ProveriPopunjenRedUTabeli(row))
                {
                    var naziv = row.Cells["Naziv"].Value.ToString();
                    var kolicina = row.Cells["Kolicina"].Value.ToString();
                    lista.Add(new Sastojak() { Naziv = naziv, Kolicina = kolicina });
                }
            }

            return lista;
        }

        private bool ProveriPopunjenRedUTabeli(DataGridViewRow row)
        {
            if ((row.Cells["Naziv"].Value != null && row.Cells["Kolicina"].Value != null) &&
                (!string.IsNullOrWhiteSpace(row.Cells["Naziv"].Value.ToString()) && !string.IsNullOrWhiteSpace(row.Cells["Kolicina"].Value.ToString())))
                return true;

            return false;
        }
    }
}
