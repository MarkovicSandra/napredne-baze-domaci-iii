﻿namespace NPB_Domaci_III
{
    partial class UpdateRecept
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSacuvaj = new System.Windows.Forms.Button();
            this.textBoxNacinPripreme = new System.Windows.Forms.TextBox();
            this.numericUpDownBrojOsoba = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDownVremePripreme = new System.Windows.Forms.NumericUpDown();
            this.comboBoxKategorija = new System.Windows.Forms.ComboBox();
            this.dataGridViewNamirnice = new System.Windows.Forms.DataGridView();
            this.Naziv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kolicina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxNaziv = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojOsoba)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVremePripreme)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNamirnice)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(99, -23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(207, 20);
            this.textBox1.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, -20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Naziv:";
            // 
            // buttonSacuvaj
            // 
            this.buttonSacuvaj.Location = new System.Drawing.Point(209, 477);
            this.buttonSacuvaj.Name = "buttonSacuvaj";
            this.buttonSacuvaj.Size = new System.Drawing.Size(75, 23);
            this.buttonSacuvaj.TabIndex = 30;
            this.buttonSacuvaj.Text = "Sacuvaj";
            this.buttonSacuvaj.UseVisualStyleBackColor = true;
            this.buttonSacuvaj.Click += new System.EventHandler(this.buttonSacuvaj_Click);
            // 
            // textBoxNacinPripreme
            // 
            this.textBoxNacinPripreme.Location = new System.Drawing.Point(135, 145);
            this.textBoxNacinPripreme.Multiline = true;
            this.textBoxNacinPripreme.Name = "textBoxNacinPripreme";
            this.textBoxNacinPripreme.Size = new System.Drawing.Size(244, 87);
            this.textBoxNacinPripreme.TabIndex = 29;
            // 
            // numericUpDownBrojOsoba
            // 
            this.numericUpDownBrojOsoba.Location = new System.Drawing.Point(135, 247);
            this.numericUpDownBrojOsoba.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDownBrojOsoba.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownBrojOsoba.Name = "numericUpDownBrojOsoba";
            this.numericUpDownBrojOsoba.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownBrojOsoba.TabIndex = 28;
            this.numericUpDownBrojOsoba.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(261, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "min";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // numericUpDownVremePripreme
            // 
            this.numericUpDownVremePripreme.Location = new System.Drawing.Point(135, 106);
            this.numericUpDownVremePripreme.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownVremePripreme.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownVremePripreme.Name = "numericUpDownVremePripreme";
            this.numericUpDownVremePripreme.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownVremePripreme.TabIndex = 26;
            this.numericUpDownVremePripreme.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // comboBoxKategorija
            // 
            this.comboBoxKategorija.FormattingEnabled = true;
            this.comboBoxKategorija.Items.AddRange(new object[] {
            "Predjelo",
            "Glavno jelo",
            "Salata",
            "Dezert"});
            this.comboBoxKategorija.Location = new System.Drawing.Point(135, 66);
            this.comboBoxKategorija.Name = "comboBoxKategorija";
            this.comboBoxKategorija.Size = new System.Drawing.Size(139, 21);
            this.comboBoxKategorija.TabIndex = 25;
            // 
            // dataGridViewNamirnice
            // 
            this.dataGridViewNamirnice.ColumnHeadersHeight = 25;
            this.dataGridViewNamirnice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Naziv,
            this.Kolicina});
            this.dataGridViewNamirnice.Location = new System.Drawing.Point(135, 285);
            this.dataGridViewNamirnice.Name = "dataGridViewNamirnice";
            this.dataGridViewNamirnice.Size = new System.Drawing.Size(244, 147);
            this.dataGridViewNamirnice.TabIndex = 24;
            // 
            // Naziv
            // 
            this.Naziv.HeaderText = "Naziv";
            this.Naziv.Name = "Naziv";
            // 
            // Kolicina
            // 
            this.Kolicina.HeaderText = "Kolicina";
            this.Kolicina.Name = "Kolicina";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(53, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Broj osoba:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(48, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Kategorija:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Vreme pripreme:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Nacin pripreme:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 275);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Namirnice:";
            // 
            // textBoxNaziv
            // 
            this.textBoxNaziv.Location = new System.Drawing.Point(135, 24);
            this.textBoxNaziv.Name = "textBoxNaziv";
            this.textBoxNaziv.Size = new System.Drawing.Size(207, 20);
            this.textBoxNaziv.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Naziv:";
            // 
            // UpdateRecept
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 543);
            this.Controls.Add(this.buttonSacuvaj);
            this.Controls.Add(this.textBoxNacinPripreme);
            this.Controls.Add(this.numericUpDownBrojOsoba);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numericUpDownVremePripreme);
            this.Controls.Add(this.comboBoxKategorija);
            this.Controls.Add(this.dataGridViewNamirnice);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxNaziv);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Name = "UpdateRecept";
            this.Text = "Azuriraj Recept";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojOsoba)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVremePripreme)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNamirnice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSacuvaj;
        private System.Windows.Forms.TextBox textBoxNacinPripreme;
        private System.Windows.Forms.NumericUpDown numericUpDownBrojOsoba;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDownVremePripreme;
        private System.Windows.Forms.ComboBox comboBoxKategorija;
        private System.Windows.Forms.DataGridView dataGridViewNamirnice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kolicina;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxNaziv;
        private System.Windows.Forms.Label label8;
    }
}