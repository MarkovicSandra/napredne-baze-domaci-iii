﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPB_Domaci_III
{
    public class Recept
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonElement]
        public string Naziv { get; set; }

        [BsonElement]
        public string Kategorija { get; set; }

        [BsonElement]
        public int VremePripreme { get; set; }

        [BsonElement]
        public string NacinPripreme { get; set; }

        [BsonElement]
        public int BrojOsoba { get; set; }

        [BsonElement]
        public List<Sastojak> Namirnice { get; set; }

        public override string ToString()
        {
            return Naziv;
        }
    }
}
