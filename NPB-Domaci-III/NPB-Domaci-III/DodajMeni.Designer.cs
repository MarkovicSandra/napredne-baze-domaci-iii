﻿namespace NPB_Domaci_III
{
    partial class DodajMeni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonSacuvaj = new System.Windows.Forms.Button();
            this.comboBoxPredjelo = new System.Windows.Forms.ComboBox();
            this.comboBoxGlavnoJelo = new System.Windows.Forms.ComboBox();
            this.comboBoxDezert = new System.Windows.Forms.ComboBox();
            this.textBoxNaziv = new System.Windows.Forms.TextBox();
            this.textBoxPice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxSalata = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Naziv:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Predjelo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Glavno jelo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Dezert:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Pice:";
            // 
            // buttonSacuvaj
            // 
            this.buttonSacuvaj.Location = new System.Drawing.Point(108, 277);
            this.buttonSacuvaj.Name = "buttonSacuvaj";
            this.buttonSacuvaj.Size = new System.Drawing.Size(75, 23);
            this.buttonSacuvaj.TabIndex = 5;
            this.buttonSacuvaj.Text = "Sacuvaj";
            this.buttonSacuvaj.UseVisualStyleBackColor = true;
            this.buttonSacuvaj.Click += new System.EventHandler(this.buttonSacuvaj_Click);
            // 
            // comboBoxPredjelo
            // 
            this.comboBoxPredjelo.FormattingEnabled = true;
            this.comboBoxPredjelo.Location = new System.Drawing.Point(94, 84);
            this.comboBoxPredjelo.Name = "comboBoxPredjelo";
            this.comboBoxPredjelo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxPredjelo.TabIndex = 7;
            // 
            // comboBoxGlavnoJelo
            // 
            this.comboBoxGlavnoJelo.FormattingEnabled = true;
            this.comboBoxGlavnoJelo.Location = new System.Drawing.Point(94, 120);
            this.comboBoxGlavnoJelo.Name = "comboBoxGlavnoJelo";
            this.comboBoxGlavnoJelo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxGlavnoJelo.TabIndex = 8;
            // 
            // comboBoxDezert
            // 
            this.comboBoxDezert.FormattingEnabled = true;
            this.comboBoxDezert.Location = new System.Drawing.Point(94, 187);
            this.comboBoxDezert.Name = "comboBoxDezert";
            this.comboBoxDezert.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDezert.TabIndex = 9;
            // 
            // textBoxNaziv
            // 
            this.textBoxNaziv.Location = new System.Drawing.Point(94, 47);
            this.textBoxNaziv.Name = "textBoxNaziv";
            this.textBoxNaziv.Size = new System.Drawing.Size(121, 20);
            this.textBoxNaziv.TabIndex = 11;
            // 
            // textBoxPice
            // 
            this.textBoxPice.Location = new System.Drawing.Point(94, 223);
            this.textBoxPice.Name = "textBoxPice";
            this.textBoxPice.Size = new System.Drawing.Size(121, 20);
            this.textBoxPice.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Salata:";
            // 
            // comboBoxSalata
            // 
            this.comboBoxSalata.FormattingEnabled = true;
            this.comboBoxSalata.Location = new System.Drawing.Point(94, 153);
            this.comboBoxSalata.Name = "comboBoxSalata";
            this.comboBoxSalata.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSalata.TabIndex = 14;
            // 
            // DodajMeni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 336);
            this.Controls.Add(this.comboBoxSalata);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxPice);
            this.Controls.Add(this.textBoxNaziv);
            this.Controls.Add(this.comboBoxDezert);
            this.Controls.Add(this.comboBoxGlavnoJelo);
            this.Controls.Add(this.comboBoxPredjelo);
            this.Controls.Add(this.buttonSacuvaj);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DodajMeni";
            this.Text = "DodajMeni";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonSacuvaj;
        private System.Windows.Forms.ComboBox comboBoxPredjelo;
        private System.Windows.Forms.ComboBox comboBoxGlavnoJelo;
        private System.Windows.Forms.ComboBox comboBoxDezert;
        private System.Windows.Forms.TextBox textBoxNaziv;
        private System.Windows.Forms.TextBox textBoxPice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxSalata;
    }
}