﻿namespace NPB_Domaci_III
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listViewRecepti = new System.Windows.Forms.ListView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listViewMeni = new System.Windows.Forms.ListView();
            this.buttonDodajRecept = new System.Windows.Forms.Button();
            this.buttonDodajMeni = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.buttonObrisiMeni = new System.Windows.Forms.Button();
            this.buttonObrisiRecept = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.buttonPretraziRecepte = new System.Windows.Forms.Button();
            this.comboBoxParametriPretrageRecepata = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.buttonPretraziMenije = new System.Windows.Forms.Button();
            this.comboBoxParametriPretrageMenija = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.textBoxPretragaRecepta = new System.Windows.Forms.TextBox();
            this.comboBoxPretragaRecepta = new System.Windows.Forms.ComboBox();
            this.textBoxPretragaMenija = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(347, 242);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listViewRecepti);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(339, 216);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Recepti";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listViewRecepti
            // 
            this.listViewRecepti.HideSelection = false;
            this.listViewRecepti.Location = new System.Drawing.Point(0, 0);
            this.listViewRecepti.Name = "listViewRecepti";
            this.listViewRecepti.Size = new System.Drawing.Size(343, 217);
            this.listViewRecepti.TabIndex = 0;
            this.listViewRecepti.UseCompatibleStateImageBehavior = false;
            this.listViewRecepti.View = System.Windows.Forms.View.List;
            this.listViewRecepti.ItemActivate += new System.EventHandler(this.listViewRecepti_ItemActivate);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listViewMeni);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(339, 216);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Meniji";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listViewMeni
            // 
            this.listViewMeni.HideSelection = false;
            this.listViewMeni.Location = new System.Drawing.Point(-4, 0);
            this.listViewMeni.Name = "listViewMeni";
            this.listViewMeni.Size = new System.Drawing.Size(343, 220);
            this.listViewMeni.TabIndex = 0;
            this.listViewMeni.UseCompatibleStateImageBehavior = false;
            this.listViewMeni.View = System.Windows.Forms.View.List;
            this.listViewMeni.ItemActivate += new System.EventHandler(this.listViewMeni_ItemActivate);
            // 
            // buttonDodajRecept
            // 
            this.buttonDodajRecept.Location = new System.Drawing.Point(27, 30);
            this.buttonDodajRecept.Name = "buttonDodajRecept";
            this.buttonDodajRecept.Size = new System.Drawing.Size(111, 31);
            this.buttonDodajRecept.TabIndex = 1;
            this.buttonDodajRecept.Text = "Dodaj recept";
            this.buttonDodajRecept.UseVisualStyleBackColor = true;
            this.buttonDodajRecept.Click += new System.EventHandler(this.buttonDodajRecept_Click);
            // 
            // buttonDodajMeni
            // 
            this.buttonDodajMeni.Location = new System.Drawing.Point(27, 78);
            this.buttonDodajMeni.Name = "buttonDodajMeni";
            this.buttonDodajMeni.Size = new System.Drawing.Size(111, 34);
            this.buttonDodajMeni.TabIndex = 2;
            this.buttonDodajMeni.Text = "Dodaj meni";
            this.buttonDodajMeni.UseVisualStyleBackColor = true;
            this.buttonDodajMeni.Click += new System.EventHandler(this.buttonDodajMeni_Click);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Location = new System.Drawing.Point(12, 291);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(347, 195);
            this.tabControl2.TabIndex = 4;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.buttonObrisiMeni);
            this.tabPage3.Controls.Add(this.buttonObrisiRecept);
            this.tabPage3.Controls.Add(this.buttonDodajRecept);
            this.tabPage3.Controls.Add(this.buttonDodajMeni);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(339, 169);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Dodaj/obrisi";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // buttonObrisiMeni
            // 
            this.buttonObrisiMeni.Location = new System.Drawing.Point(183, 78);
            this.buttonObrisiMeni.Name = "buttonObrisiMeni";
            this.buttonObrisiMeni.Size = new System.Drawing.Size(116, 34);
            this.buttonObrisiMeni.TabIndex = 4;
            this.buttonObrisiMeni.Text = "Obrisi izabrani meni";
            this.buttonObrisiMeni.UseVisualStyleBackColor = true;
            this.buttonObrisiMeni.Click += new System.EventHandler(this.buttonObrisiMeni_Click);
            // 
            // buttonObrisiRecept
            // 
            this.buttonObrisiRecept.Location = new System.Drawing.Point(183, 30);
            this.buttonObrisiRecept.Name = "buttonObrisiRecept";
            this.buttonObrisiRecept.Size = new System.Drawing.Size(116, 31);
            this.buttonObrisiRecept.TabIndex = 3;
            this.buttonObrisiRecept.Text = "Obrisi izabrani recept";
            this.buttonObrisiRecept.UseVisualStyleBackColor = true;
            this.buttonObrisiRecept.Click += new System.EventHandler(this.buttonObrisiRecept_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.comboBoxPretragaRecepta);
            this.tabPage4.Controls.Add(this.textBoxPretragaRecepta);
            this.tabPage4.Controls.Add(this.buttonPretraziRecepte);
            this.tabPage4.Controls.Add(this.comboBoxParametriPretrageRecepata);
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(339, 169);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Pretraga recepata";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // buttonPretraziRecepte
            // 
            this.buttonPretraziRecepte.Location = new System.Drawing.Point(135, 116);
            this.buttonPretraziRecepte.Name = "buttonPretraziRecepte";
            this.buttonPretraziRecepte.Size = new System.Drawing.Size(75, 23);
            this.buttonPretraziRecepte.TabIndex = 2;
            this.buttonPretraziRecepte.Text = "Pretrazi";
            this.buttonPretraziRecepte.UseVisualStyleBackColor = true;
            this.buttonPretraziRecepte.Click += new System.EventHandler(this.buttonPretraziRecepte_Click);
            // 
            // comboBoxParametriPretrageRecepata
            // 
            this.comboBoxParametriPretrageRecepata.FormattingEnabled = true;
            this.comboBoxParametriPretrageRecepata.Items.AddRange(new object[] {
            "Naziv",
            "Kategorija"});
            this.comboBoxParametriPretrageRecepata.Location = new System.Drawing.Point(135, 21);
            this.comboBoxParametriPretrageRecepata.Name = "comboBoxParametriPretrageRecepata";
            this.comboBoxParametriPretrageRecepata.Size = new System.Drawing.Size(121, 21);
            this.comboBoxParametriPretrageRecepata.TabIndex = 1;
            this.comboBoxParametriPretrageRecepata.SelectedIndexChanged += new System.EventHandler(this.comboBoxParametriPretrageRecepata_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Parametar pretrage:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.textBoxPretragaMenija);
            this.tabPage5.Controls.Add(this.buttonPretraziMenije);
            this.tabPage5.Controls.Add(this.comboBoxParametriPretrageMenija);
            this.tabPage5.Controls.Add(this.label2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(339, 169);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Pretraga menija";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // buttonPretraziMenije
            // 
            this.buttonPretraziMenije.Location = new System.Drawing.Point(139, 119);
            this.buttonPretraziMenije.Name = "buttonPretraziMenije";
            this.buttonPretraziMenije.Size = new System.Drawing.Size(75, 23);
            this.buttonPretraziMenije.TabIndex = 2;
            this.buttonPretraziMenije.Text = "Pretrazi";
            this.buttonPretraziMenije.UseVisualStyleBackColor = true;
            this.buttonPretraziMenije.Click += new System.EventHandler(this.buttonPretraziMenije_Click);
            // 
            // comboBoxParametriPretrageMenija
            // 
            this.comboBoxParametriPretrageMenija.FormattingEnabled = true;
            this.comboBoxParametriPretrageMenija.Items.AddRange(new object[] {
            "Naziv",
            "Predjelo",
            "GlavnoJelo",
            "Salata",
            "Dezert",
            "Pice"});
            this.comboBoxParametriPretrageMenija.Location = new System.Drawing.Point(139, 27);
            this.comboBoxParametriPretrageMenija.Name = "comboBoxParametriPretrageMenija";
            this.comboBoxParametriPretrageMenija.Size = new System.Drawing.Size(121, 21);
            this.comboBoxParametriPretrageMenija.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Parametar pretrage:";
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(147, 262);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(75, 23);
            this.buttonRefresh.TabIndex = 5;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // textBoxPretragaRecepta
            // 
            this.textBoxPretragaRecepta.Location = new System.Drawing.Point(135, 66);
            this.textBoxPretragaRecepta.Name = "textBoxPretragaRecepta";
            this.textBoxPretragaRecepta.Size = new System.Drawing.Size(121, 20);
            this.textBoxPretragaRecepta.TabIndex = 3;
            // 
            // comboBoxPretragaRecepta
            // 
            this.comboBoxPretragaRecepta.FormattingEnabled = true;
            this.comboBoxPretragaRecepta.Items.AddRange(new object[] {
            "Predjelo",
            "Glavno jelo",
            "Salata",
            "Dezert"});
            this.comboBoxPretragaRecepta.Location = new System.Drawing.Point(135, 65);
            this.comboBoxPretragaRecepta.Name = "comboBoxPretragaRecepta";
            this.comboBoxPretragaRecepta.Size = new System.Drawing.Size(121, 21);
            this.comboBoxPretragaRecepta.TabIndex = 4;
            // 
            // textBoxPretragaMenija
            // 
            this.textBoxPretragaMenija.Location = new System.Drawing.Point(139, 65);
            this.textBoxPretragaMenija.Name = "textBoxPretragaMenija";
            this.textBoxPretragaMenija.Size = new System.Drawing.Size(121, 20);
            this.textBoxPretragaMenija.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 489);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Recipe book";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView listViewRecepti;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView listViewMeni;
        private System.Windows.Forms.Button buttonDodajRecept;
        private System.Windows.Forms.Button buttonDodajMeni;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox comboBoxParametriPretrageRecepata;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button buttonPretraziRecepte;
        private System.Windows.Forms.Button buttonPretraziMenije;
        private System.Windows.Forms.ComboBox comboBoxParametriPretrageMenija;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonObrisiMeni;
        private System.Windows.Forms.Button buttonObrisiRecept;
        private System.Windows.Forms.ComboBox comboBoxPretragaRecepta;
        private System.Windows.Forms.TextBox textBoxPretragaRecepta;
        private System.Windows.Forms.TextBox textBoxPretragaMenija;
    }
}

