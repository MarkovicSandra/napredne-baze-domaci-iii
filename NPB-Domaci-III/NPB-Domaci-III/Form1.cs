﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;
using Newtonsoft.Json;
using MongoDB.Bson.Serialization;
using NPB_Domaci_III.Data_Layer;

namespace NPB_Domaci_III
{
    public partial class Form1 : Form
    {
        DataProvider dataProvider = new DataProvider();
        public Form1()
        {
            InitializeComponent();
            DodajRecepteUListu();
            DodajMenijeUListu();
            textBoxPretragaRecepta.Visible = false;
            comboBoxPretragaRecepta.Visible = false;
        }

        private void DodajRecepteUListu()
        {
            var recepti = dataProvider.VratiSveRecepte();

            foreach (var recept in recepti)
                listViewRecepti.Items.Add(recept.ToString());
        }

        private void DodajMenijeUListu()
        {
            var meniji = dataProvider.VratiSveMenije();

            foreach (var meni in meniji)
                listViewMeni.Items.Add(meni.ToString());
        }

        private void buttonDodajRecept_Click(object sender, EventArgs e)
        {
            DodajRecept form = new DodajRecept();
            form.Show();
        }

        private void buttonDodajMeni_Click(object sender, EventArgs e)
        {
            DodajMeni form = new DodajMeni();
            form.Show();
        }


        private void buttonPretraziRecepte_Click(object sender, EventArgs e)
        {
            if (comboBoxParametriPretrageRecepata.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati parametar pretrage!");
                return;
            }

            string parametarPretrage = comboBoxParametriPretrageRecepata.SelectedItem.ToString();
            string vrednostPretrage;

            if (comboBoxParametriPretrageRecepata.SelectedIndex == 1)
            {
                if (comboBoxPretragaRecepta.SelectedIndex == -1)
                {
                    MessageBox.Show("Morate izabrati kategoriju za pretragu!");
                    return;
                }

                vrednostPretrage = comboBoxPretragaRecepta.SelectedItem.ToString();
            }
            else
            {
                 string text = textBoxPretragaRecepta.Text;

                 if (string.IsNullOrEmpty(text))
                 {
                     MessageBox.Show("Morate uneti naziv za pretragu!");
                     return;
                 }
                 else
                     vrednostPretrage = text;
            }

            var rezultatPretrage = dataProvider.VratiSveRecepte(parametarPretrage, vrednostPretrage);

            foreach (ListViewItem item in listViewRecepti.Items)
                listViewRecepti.Items.Remove(item);

            foreach (var item in rezultatPretrage)
                listViewRecepti.Items.Add(new ListViewItem(item.ToString()));

        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listViewRecepti.Items)
                listViewRecepti.Items.Remove(item);

            foreach (ListViewItem item in listViewMeni.Items)
                listViewMeni.Items.Remove(item);

            DodajRecepteUListu();
            DodajMenijeUListu();
        }

        private void listViewRecepti_ItemActivate(object sender, EventArgs e)
        {
            string izabranRecept = listViewRecepti.FocusedItem.Text;

            var recept = dataProvider.VratiRecept(izabranRecept);

            var updateRecept = new UpdateRecept(recept);

            updateRecept.Show();
        }

        private void buttonObrisiRecept_Click(object sender, EventArgs e)
        {
            var izabraniRecept = listViewRecepti.FocusedItem;

            if (izabraniRecept == null)
            {
                MessageBox.Show("Morate izabrati recept za brisanje!");
                return;
            }

            string nazivIzabranogRecepta = izabraniRecept.Text;

            dataProvider.ObrisiRecept(nazivIzabranogRecepta);

            foreach (ListViewItem item in listViewRecepti.Items)
                listViewRecepti.Items.Remove(item);

            DodajRecepteUListu();
        }

        private void buttonObrisiMeni_Click(object sender, EventArgs e)
        {
            var izabraniMeni = listViewMeni.FocusedItem;

            if (izabraniMeni == null)
            {
                MessageBox.Show("Morate izabrati meni za brisanje!");
                return;
            }

            string nazivIzabranogMenija = izabraniMeni.Text;

            dataProvider.ObrisiMeni(nazivIzabranogMenija);

            foreach (ListViewItem item in listViewMeni.Items)
                listViewMeni.Items.Remove(item);

            DodajMenijeUListu();
        }

        private void listViewMeni_ItemActivate(object sender, EventArgs e)
        {
            string izabranMeni = listViewMeni.FocusedItem.Text;

            var recept = dataProvider.VratiMeni(izabranMeni);

            var updateMeni = new UpdateMeni(recept);

            updateMeni.Show();
        }

        private void comboBoxParametriPretrageRecepata_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxParametriPretrageRecepata.SelectedIndex == 1)
            {
                comboBoxPretragaRecepta.Visible = true;
                textBoxPretragaRecepta.Visible = false;
            }
            else
            {
                textBoxPretragaRecepta.Visible = true;
                comboBoxPretragaRecepta.Visible = false;
            }
        }

        private void buttonPretraziMenije_Click(object sender, EventArgs e)
        {
            if (comboBoxParametriPretrageMenija.SelectedIndex == -1)
            {
                MessageBox.Show("Morate izabrati parametar pretrage!");
                return;
            }

            string parametarPretrage = comboBoxParametriPretrageMenija.SelectedItem.ToString();
            string vrednostPretrage = textBoxPretragaMenija.Text;

            if (string.IsNullOrEmpty(vrednostPretrage))
            {
                MessageBox.Show("Morate uneti vrednost za pretragu!");
                return;
            }

            var rezultatPretrage = dataProvider.VratiSveMenije(parametarPretrage, vrednostPretrage);

            foreach (ListViewItem item in listViewMeni.Items)
                listViewMeni.Items.Remove(item);

            foreach (var item in rezultatPretrage)
                listViewMeni.Items.Add(new ListViewItem(item.ToString()));
        }
    }
}
