﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPB_Domaci_III
{
    public class Meni
    {
        [BsonId]
        public ObjectId _id { get; set; }

        [BsonElement]
        public string Naziv { get; set; }

        [BsonElement]
        public string Predjelo { get; set; }

        [BsonElement]
        public string GlavnoJelo { get; set; }

        [BsonElement]
        public string Salata { get; set; }

        [BsonElement]
        public string Dezert { get; set; }

        [BsonElement]
        public string Pice { get; set; }

        public override string ToString()
        {
            return Naziv;
        }
    }
}
