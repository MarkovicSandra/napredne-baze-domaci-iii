﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPB_Domaci_III.Data_Layer
{
    public class DataProvider
    {
        private MongoClient client;
        private IMongoDatabase database;

        public DataProvider()
        {
            client = new MongoClient();
            database = client.GetDatabase("");
        }

        public List<Recept> VratiSveRecepte()
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            var filter = new BsonDocument();
            var result = collection.Find(filter).ToList();

            List<Recept> list = new List<Recept>();

            foreach (var a in result)
                list.Add(BsonSerializer.Deserialize<Recept>(a.ToJson()));

            return list;
        }

        public List<Recept> VratiSveRecepte(string parametarPretrage, string vrednostPretrage)
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            var filter = Builders<BsonDocument>.Filter.Eq(parametarPretrage, vrednostPretrage);
            var result = collection.Find(filter).ToList();

            List<Recept> list = new List<Recept>();

            foreach (var a in result)
                list.Add(BsonSerializer.Deserialize<Recept>(a.ToJson()));

            return list;
        }

        public bool PostojiRecept(string naziv)
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            var filter = Builders<BsonDocument>.Filter.Eq("Naziv", naziv);

            var result = collection.Find(filter).ToList();

            if (result.Count >= 1)
                return true;
            else
                return false;
        }

        public void DodajNoviRecept(Recept recept)
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            collection.InsertOne(KreirajBsonDokument(recept));
        }

        public Recept VratiRecept(string naziv)
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            var filter = Builders<BsonDocument>.Filter.Eq("Naziv", naziv);
            var result = collection.Find(filter).ToList();

            return BsonSerializer.Deserialize<Recept>(result[0].ToJson());
        }

        private BsonDocument KreirajBsonDokument(Recept recept)
        {
            BsonArray namirnice = new BsonArray();

            foreach (var namirnica in recept.Namirnice)
            {
                var json = JsonConvert.SerializeObject(namirnica);
                namirnice.Add(BsonDocument.Parse(json));
            }

            var document = new BsonDocument
                {
                    {"Naziv", recept.Naziv},
                    {"Kategorija", recept.Kategorija },
                    {"VremePripreme", recept.VremePripreme },
                    {"NacinPripreme", recept.NacinPripreme },
                    {"BrojOsoba", recept.BrojOsoba },
                    {"Namirnice", namirnice}
                };

            return document;
        }

        public void UpdateRecept(Recept recept, string naziv)
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            var filter = Builders<BsonDocument>.Filter.Eq("Naziv", naziv);

            BsonArray namirnice = new BsonArray();

            foreach (var namirnica in recept.Namirnice)
            {
                var json = JsonConvert.SerializeObject(namirnica);
                namirnice.Add(BsonDocument.Parse(json));
            }

            var update = Builders<BsonDocument>.Update
                .Set("Naziv", recept.Naziv)
                .Set("Kategorija", recept.Kategorija)
                .Set("VremePripreme", recept.VremePripreme)
                .Set("NacinPripreme", recept.NacinPripreme)
                .Set("BrojOsoba", recept.BrojOsoba)
                .Set("Namirnice", namirnice);

            collection.UpdateOne(filter, update);
        }

        public void ObrisiRecept(string naziv)
        {
            var collection= database.GetCollection<BsonDocument>("Recepti");
            var filter = Builders<BsonDocument>.Filter.Eq("Naziv", naziv);
            var result = collection.Find(filter).ToList();

            collection.DeleteOne(filter);
        }

        public List<Meni> VratiSveMenije()
        {
            var collection = database.GetCollection<BsonDocument>("Meniji");
            var filter = new BsonDocument();
            var result = collection.Find(filter).ToList();

            List<Meni> list = new List<Meni>();

            foreach (var a in result)
                list.Add(BsonSerializer.Deserialize<Meni>(a.ToJson()));

            return list;
        }

        public bool PostojiMeni(string naziv)
        {
            var collection = database.GetCollection<BsonDocument>("Meniji");
            var filter = Builders<BsonDocument>.Filter.Eq("Naziv", naziv);

            var result = collection.Find(filter).ToList();

            if (result.Count >= 1)
                return true;
            else
                return false;
        }

        public List<string> VratiSvaPredjela()
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            var filter = Builders<BsonDocument>.Filter.Eq("Kategorija", "Predjelo");

            var result = collection.Find(filter).ToList();
            var predjela = new List<string>();

            foreach (var a in result)
            {
                var recept = a.GetValue("Naziv").ToString();
                predjela.Add(recept);
            }

            return predjela;
        }

        public List<string> VratiSvaGlavnaJela()
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            var filter = Builders<BsonDocument>.Filter.Eq("Kategorija", "Glavno jelo");

            var result = collection.Find(filter).ToList();
            var glavnaJela = new List<string>();

            foreach (var a in result)
            {
                var recept = a.GetValue("Naziv").ToString();
                glavnaJela.Add(recept);
            }

            return glavnaJela;
        }

        public List<string> VratiSveDezerte()
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            var filter = Builders<BsonDocument>.Filter.Eq("Kategorija", "Dezert");

            var result = collection.Find(filter).ToList();
            var dezerti = new List<string>();

            foreach (var a in result)
            {
                var recept = a.GetValue("Naziv").ToString();
                dezerti.Add(recept);
            }

            return dezerti;
        }

        public List<string> VratiSveSalate()
        {
            var collection = database.GetCollection<BsonDocument>("Recepti");
            var filter = Builders<BsonDocument>.Filter.Eq("Kategorija", "Salata");

            var result = collection.Find(filter).ToList();
            var salate = new List<string>();

            foreach (var a in result)
            {
                var recept = a.GetValue("Naziv").ToString();
                salate.Add(recept);
            }

            return salate;
        }

        public void DodajNoviMeni(Meni meni)
        {
            var collection = database.GetCollection<BsonDocument>("Meniji");
            collection.InsertOne(KreirajBsonDokument(meni));
        }

        private BsonDocument KreirajBsonDokument(Meni meni)
        {

            var document = new BsonDocument
                {
                    {"Naziv", meni.Naziv},
                    {"Predjelo", meni.Predjelo },
                    {"GlavnoJelo", meni.GlavnoJelo },
                    {"Salata", meni.Salata },
                    {"Dezert", meni.Dezert },
                    {"Pice", meni.Pice}
                };

            return document;
        }

        public void ObrisiMeni(string naziv)
        {
            var collection = database.GetCollection<BsonDocument>("Meniji");
            var filter = Builders<BsonDocument>.Filter.Eq("Naziv", naziv);

            collection.DeleteOne(filter);
        }

        public Meni VratiMeni(string naziv)
        {
            var collection = database.GetCollection<BsonDocument>("Meniji");
            var filter = Builders<BsonDocument>.Filter.Eq("Naziv", naziv);
            var result = collection.Find(filter).ToList();

            return BsonSerializer.Deserialize<Meni>(result[0].ToJson());
        }

        public void UpdateMeni(Meni meni, string naziv)
        {
            var collection = database.GetCollection<BsonDocument>("Meniji");
            var filter = Builders<BsonDocument>.Filter.Eq("Naziv", naziv);


            var update = Builders<BsonDocument>.Update
                .Set("Naziv", meni.Naziv)
                .Set("Predjelo", meni.Predjelo)
                .Set("GlavnoJelo", meni.GlavnoJelo)
                .Set("Salata", meni.Salata)
                .Set("Dezert", meni.Dezert)
                .Set("Pice", meni.Pice);

            collection.UpdateOne(filter, update);
        }

        public List<Meni> VratiSveMenije(string parametarPretrage, string vrednostPretrage)
        {
            var collection = database.GetCollection<BsonDocument>("Meniji");
            var filter = Builders<BsonDocument>.Filter.Eq(parametarPretrage, vrednostPretrage);
            var result = collection.Find(filter).ToList();

            List<Meni> list = new List<Meni>();

            foreach (var a in result)
                list.Add(BsonSerializer.Deserialize<Meni>(a.ToJson()));

            return list;
        }
    }
}
